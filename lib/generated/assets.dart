///This file is automatically generated. DO NOT EDIT, all your changes would be lost.
class Assets {
  Assets._();

  static const String imagesLogo = 'assets/images/logo.png';
  static const String iransansBlack = 'assets/fonts/iransans/black.ttf';
  static const String iransansBold = 'assets/fonts/iransans/bold.ttf';
  static const String iransansLight = 'assets/fonts/iransans/light.ttf';
  static const String iransansMedium = 'assets/fonts/iransans/medium.ttf';
  static const String iransansRegular = 'assets/fonts/iransans/regular.ttf';
  static const String iransansUltraLight =
      'assets/fonts/iransans/ultraLight.ttf';
}
