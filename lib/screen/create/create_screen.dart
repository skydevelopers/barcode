import 'dart:math';

import 'package:barcode/service/permission-handler.dart';
import 'package:convert_widget_to_image/widget_to_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pretty_qr_code/pretty_qr_code.dart';
import 'package:share_plus/share_plus.dart';

class CreateScreen extends StatefulWidget {
  const CreateScreen({
    super.key,
  });

  @override
  State<CreateScreen> createState() => _CreateScreenState();
}

class _CreateScreenState extends State<CreateScreen> {
  TextEditingController controller = TextEditingController();
  ByteData? byteData;

  GlobalKey key = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Create Barcode'),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            RepaintBoundary(
              key: key,
              child: Container(
                color: Colors.white,
                padding: const EdgeInsets.symmetric(
                  horizontal: 32,
                  vertical: 32,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    PrettyQr(
                      size: MediaQuery.of(context).size.width * 0.6,
                      data: controller.text,
                      errorCorrectLevel: QrErrorCorrectLevel.M,
                      typeNumber: null,
                      roundEdges: true,
                    ),
                  ],
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 16,
                vertical: 32,
              ),
              margin: const EdgeInsets.fromLTRB(16, 32, 16, 16),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: Colors.white,
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black12,
                    blurRadius: 2,
                  ),
                ],
              ),
              child: Column(
                children: [
                  TextField(
                    controller: controller,
                    autofocus: true,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(6),
                      ),
                      labelText: 'Data',
                      alignLabelWithHint: true,
                      hintText: 'Enter your text',
                      hintStyle: TextStyle(
                        color: Colors.grey[800],
                      ),
                      fillColor: Colors.white70,
                    ),
                    keyboardType: TextInputType.multiline,
                    onChanged: (text) {
                      setState(() {});
                    },
                  ),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                InkWell(
                  onTap: () async {
                    final PermissionStatus status =
                        await PermissionHandler.getStoragePermission();

                    if (status.isGranted) {
                      final result = await ImageGallerySaver.saveImage(
                        await WidgetToImage.asUint8List(key),
                        quality: 100,
                        name:
                            '${10 + Random().nextInt((50 + 1) - 10) + DateTime.now().millisecondsSinceEpoch}',
                      );
                      if (mounted) {
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                            content: Text('File Saved!'),
                          ),
                        );
                      }
                    } else {
                      await openAppSettings();
                    }
                  },
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 24,
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      color: Colors.white,
                      boxShadow: const [
                        BoxShadow(
                          color: Colors.black12,
                          blurRadius: 2,
                        ),
                      ],
                    ),
                    child: const Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.save,
                          size: 24,
                        ),
                        SizedBox(height: 8),
                        Text('Save')
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () async {
                    final image = await WidgetToImage.asUint8List(key);
                    await Share.shareXFiles(
                      [
                        XFile.fromData(
                          image,
                          name:
                              '${10 + Random().nextInt((50 + 1) - 10) + DateTime.now().millisecondsSinceEpoch}',
                          mimeType: 'jpg',
                          length: image.lengthInBytes,
                        ),
                      ],
                    );
                  },
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 24,
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      color: Colors.white,
                      boxShadow: const [
                        BoxShadow(
                          color: Colors.black12,
                          blurRadius: 2,
                        ),
                      ],
                    ),
                    child: const Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.share,
                          size: 24,
                        ),
                        SizedBox(height: 8),
                        Text('Share')
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}
