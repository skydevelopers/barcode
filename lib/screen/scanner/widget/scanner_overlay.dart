import 'package:flutter/material.dart';

class ScannerOverlay extends CustomPainter {
  ScannerOverlay({
    required this.width,
    required this.height,
    this.radius = 0,
    this.trackerOpacity = 1.0,
    this.strokeWidth = 4.0,
    this.borderColor = Colors.amber,
    required this.backgroundColor,
  });

  final double width;
  final double height;
  final double radius;
  final double trackerOpacity;
  final double strokeWidth;
  final Color borderColor;
  final Color backgroundColor;

  @override
  void paint(Canvas canvas, Size size) {
    double w = size.width;
    double h = size.height;

    final backgroundPath = Path()..addRect(Rect.largest);
    final cutoutPath = Path()
      ..addRRect(
        RRect.fromRectAndRadius(
          Rect.fromCenter(
            center: Offset(w / 2, h / 2),
            width: width,
            height: height,
          ),
          Radius.circular(radius),
        ),
      );

    final backgroundPaint = Paint()
      ..color = backgroundColor
      ..style = PaintingStyle.fill
      ..blendMode = BlendMode.dstOut;

    final backgroundWithCutout = Path.combine(
      PathOperation.difference,
      backgroundPath,
      cutoutPath,
    );

    final borderPaint = Paint()
      ..color = borderColor
      ..style = PaintingStyle.stroke
      ..strokeWidth = strokeWidth
      ..strokeCap = StrokeCap.round;

    debugPrint((trackerOpacity * height).toString());
    Offset p1 = Offset(w / 2 - width / 2 + width * 12 / 100,
        h / 2 - height / 2 + trackerOpacity * height);
    Offset p2 = Offset(w / 2 + width / 2 - width * 12 / 100,
        h / 2 - height / 2 + trackerOpacity * height);
    final paint = Paint()
      ..color = borderColor.withOpacity(1.0)
      ..strokeWidth = strokeWidth / 1.3
      ..strokeCap = StrokeCap.round;

    canvas.drawPath(backgroundWithCutout, backgroundPaint);
    canvas.drawPath(backgroundWithCutout, borderPaint);
    canvas.drawLine(p1, p2, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
