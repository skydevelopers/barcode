import 'package:barcode/screen/create/create_screen.dart';
import 'package:barcode/screen/scan_result/scan_result_screen.dart';
import 'package:barcode/screen/scanner/widget/scanner_overlay.dart';
import 'package:flutter/material.dart';
import 'package:mobile_scanner/mobile_scanner.dart';

class ScannerScreen extends StatefulWidget {
  const ScannerScreen({Key? key}) : super(key: key);

  @override
  State<ScannerScreen> createState() => _ScannerScreenState();
}

class _ScannerScreenState extends State<ScannerScreen>
    with WidgetsBindingObserver, SingleTickerProviderStateMixin {
  MobileScannerController cameraController = MobileScannerController();
  bool reAuthenticate = false;

  MobileScannerArguments? arguments;

  bool isVisible = false;
  double begin = 0.2;
  double end = 0.8;

  @override
  void initState() {
    cameraController.stop();
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    cameraController.stop();
    cameraController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final scanWindow = Rect.fromCenter(
      center: MediaQuery.of(context).size.center(Offset.zero),
      width: MediaQuery.of(context).size.width / 2,
      height: MediaQuery.of(context).size.width / 2,
    );
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: const Text('Barcode Scanner'),
        actions: [
          IconButton(
            icon: ValueListenableBuilder(
              valueListenable: cameraController.torchState,
              builder: (context, state, child) {
                switch (state) {
                  case TorchState.off:
                    return const Icon(Icons.flash_off, color: Colors.grey);
                  case TorchState.on:
                    return const Icon(Icons.flash_off, color: Colors.yellow);
                }
              },
            ),
            onPressed: () {
              cameraController.toggleTorch();
            },
          ),
          IconButton(
            icon: ValueListenableBuilder(
              valueListenable: cameraController.cameraFacingState,
              builder: (context, state, child) {
                switch (state) {
                  case CameraFacing.front:
                    return const Icon(Icons.camera_front);
                  case CameraFacing.back:
                    return const Icon(Icons.camera_rear);
                }
              },
            ),
            onPressed: () {
              cameraController.switchCamera();
            },
          ),
        ],
      ),
      body: Stack(
        fit: StackFit.expand,
        children: [
          MobileScanner(
            controller: cameraController,
            scanWindow: scanWindow,
            onDetect: onDetect,
            onScannerStarted: (arguments) {
              setState(() {
                this.arguments = arguments;
              });
            },
          ),
          Builder(
            builder: (context) {
              return TweenAnimationBuilder<double>(
                tween: Tween<double>(
                  begin: !isVisible ? begin : end,
                  end: !isVisible ? end : begin,
                ),
                curve: Curves.linear,
                duration: const Duration(milliseconds: 1500),
                onEnd: () {
                  setState(() {
                    isVisible = !isVisible;
                  });
                },
                builder: (context, opacity, child) {
                  return CustomPaint(
                    painter: ScannerOverlay(
                      width: scanWindow.width,
                      height: scanWindow.height,
                      radius: 32,
                      borderColor: Colors.amber,
                      strokeWidth: 4,
                      trackerOpacity: opacity,
                      backgroundColor: Colors.indigo.withOpacity(0.5),
                    ),
                  );
                },
              );
            },
          ),
          Positioned(
            bottom: 16,
            right: 64,
            left: 64,
            child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.pink),
              ),
              onPressed: () {
                cameraController.stop();
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => const CreateScreen(),
                  ),
                );

                cameraController.start();
              },
              child: const Text(
                'New barcode',
                style: TextStyle(fontSize: 16, color: Colors.white),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void onDetect(BarcodeCapture capture) {
    cameraController.stop();
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => ScanResultScreen(
          barcode: capture.barcodes.first,
        ),
      ),
    );
    cameraController.start();
  }
}
